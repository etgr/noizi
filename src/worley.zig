const std = @import("std");

const Alloc = std.mem.Allocator;
const AList = std.ArrayList;
const Random = std.rand.Random;
const sort = std.sort.sort;

pub const Point = struct {
    const Self = @This();

    const PType = f32;

    x: PType = 0,
    y: PType = 0,
    z: PType = 0,
    t: PType = 0,

    pub fn random(self: *Self, r: *Random) void {
        self.x = r.float(PType);
        self.y = r.float(PType);
        self.z = r.float(PType);
        self.t = r.float(PType);
    }

    pub fn distSqrTo(self: Self, comptime T: type, other: Self) T {
        var sum = @as(T, 0);

        inline for (@typeInfo(Self).Struct.fields) |f| {
            const diff = @field(self, f.name) - @field(other, f.name);
            sum += diff * diff;
        }

        return sum;
    }

    pub fn distTo(self: Self, comptime T: type, other: Self) T {
        return std.math.sqrt(self.distSqrTo(T, other));
    }
};

pub const Worley = struct {
    const Self = @This();

    alloc: Alloc,

    dists: AList(f32),
    features: AList(Point),
    nth_closest: usize,

    pub fn init(allocator: Alloc, n: usize) Self {
        return Self{
            .alloc = allocator,

            .dists = AList(f32).init(allocator),
            .features = AList(Point).init(allocator),
            .nth_closest = n,
        };
    }

    pub fn deinit(self: *Self) void {
        self.dists.deinit();
        self.features.deinit();
    }

    pub fn genFeatures(self: *Self, r: *Random, num: usize) !void {
        var i: usize = 0;
        while (i < num) : (i += 1) {
            var p = Point{};
            p.random(r);
            try self.features.append(p);
        }
    }

    pub fn noise(self: *Self, point: Point, dist_sqr: bool) !f32 {
        if (self.features.items.len == 0 or
            self.nth_closest > self.features.items.len)
            return error.NotEnoughFeatures;

        // Ensure that the distance list has the right number of elements
        try self.dists.ensureTotalCapacity(self.features.items.len);
        while (self.dists.items.len < self.features.items.len) {
            try self.dists.append(0);
        }

        // Calculate the distances from the current point to each feature
        for (self.features.items) |feature, i| {
            self.dists.items[i] = if (dist_sqr)
                feature.distSqrTo(f32, point)
            else
                feature.distTo(f32, point);
        }

        // sort the features according to how close they are to the selected
        // point
        sort(f32, self.dists.items, 0, lessThan);

        // Return the distance between the nth-closest and the selected point

        return self.dists.items[self.nth_closest];
    }
};

fn lessThan(comptime _: comptime_int, lhs: f32, rhs: f32) bool {
    return lhs < rhs;
}
