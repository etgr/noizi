const std = @import("std");

const Builder = std.build.Builder;

const EXAMPLES_DIRNAME = "examples";

const Example = struct {
    name: []const u8,
    desc: []const u8,
};

const EXAMPLES = [_]Example{.{
    .name = "worley",
    .desc = "Simple demonstration of Worley noise",
}};

pub fn build(b: *Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();
    const lib = b.addStaticLibrary("noizi", "src/main.zig");
    lib.setBuildMode(mode);
    lib.install();

    var main_tests = b.addTest("src/main.zig");
    main_tests.setBuildMode(mode);

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);

    inline for (EXAMPLES) |example| {
        const ex = b.addExecutable(
            example.name,
            EXAMPLES_DIRNAME ++ "/" ++ example.name ++ ".zig",
        );
        ex.setTarget(target);
        ex.setBuildMode(mode);

        ex.addPackagePath("noizi", "src/main.zig");
        ex.linkLibC();
        ex.linkSystemLibrary("SDL2");
        ex.linkSystemLibrary("SDL2_ttf");
        ex.linkSystemLibrary("SDL2_gfx");

        const run_step = b.step(example.name, example.desc);
        run_step.dependOn(&ex.run().step);
    }
}
