# noizi

A noise library for [Zig][]. Just don't expect much of it.

[Zig]: https://ziglang.org/

## Examples

This repository also contains simple visualizations for each algorithm, but
they depend on [SDL2][] and some extensions, mainly [SDL2_ttf][] and
[SDL2_gfx][].

Each example can be built simply by calling

```shell
zig build <example_name> -Drelease-fast
```

and you can check which are available with

```shell
zig build --help
```

[SDL2]: https://www.libsdl.org/
[SDL2_ttf]: https://www.libsdl.org/projects/SDL_ttf/
[SDL2_gfx]: https://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/

## External resources

Although without any affiliation, some external resources are bundled with this
library to make the examples a bit more interesting.

The complete list of bundled resources, as well as the place each was obtained
and the creator's webpage follows.

### Fonts

- [8bitOperatorPlus][]: <https://grandchaos9000.deviantart.com/> (dead link?)

[8bitOperatorPlus]: https://www.1001freefonts.com/8-bit-operator.font
