const z = @import("std");

const Alloc = z.mem.Allocator;
const Rand = z.rand.Random;

const Worley = @import("noizi").Worley;

const s = @import("shared/c.zig").sdl;
const math = @import("shared/math.zig");

const Window = @import("shared/window.zig").Window;

const WINDOW_CONFIG = .{
    .w = 800,
    .h = 600,
    .title = "Worley noise example",
};

pub const log = @import("shared/log.zig").log;
pub const log_level = .info;

const use_square_distance = true;
const max_dist: f32 = 250 * 250;

var win: Window = undefined;

var alloc: Alloc = undefined;
var randgen: Rand = undefined;

var render_features: bool = false;

var worley: Worley = undefined;

var noise_cache = [_]u8{0} ** (WINDOW_CONFIG.w * WINDOW_CONFIG.h);

pub fn main() void {
    var arena = z.heap.ArenaAllocator.init(z.heap.page_allocator);
    defer arena.deinit();

    alloc = arena.allocator();
    randgen = z.rand.DefaultPrng.init(0).random();

    worley = Worley.init(alloc, 0);

    win = Window.init(
        WINDOW_CONFIG.w,
        WINDOW_CONFIG.h,
        WINDOW_CONFIG.title,
    ) catch {
        z.log.warn("{s}\n", .{@import("shared/c.zig").sdl.SDL_GetError()});
        @panic("");
    };
    defer win.deinit();

    recalculateNoise();
    win.loop();
}

pub fn update(_: u64) void {
    const l = z.log.scoped(.update);
    l.debug("entering update", .{});

    const keyboard = @import("shared/keyboard.zig");

    if (keyboard.isKeyDown("quit")) {
        l.debug("key quit pressed", .{});

        win.running = false;
        return;
    }

    if (keyboard.isKeyDown("reload")) {
        l.debug("key reload pressed", .{});

        reload();
    }

    if (keyboard.isKeyDown("render_features")) {
        l.debug("key render_features pressed", .{});

        render_features = !render_features;
        win.should_redraw = true;
    }

    const mouse = @import("shared/mouse.zig");

    if (mouse.isButtonDown("main")) {
        const pos = mouse.pos();

        l.debug("main mouse button pressed at {}", .{pos});

        worley.features.append(.{
            .x = @intToFloat(f32, pos.x),
            .y = @intToFloat(f32, pos.y),
        }) catch unreachable;

        recalculateNoise();
    }
}

pub fn deinit() void {
    const l = z.log.scoped(.deinit);
    l.debug("entering deinit", .{});

    worley.deinit();
}

pub fn render(renderer: *s.SDL_Renderer) void {
    const l = z.log.scoped(.render);
    l.debug("entering render", .{});

    var row: c_int = 0;
    while (row < WINDOW_CONFIG.h) : (row += 1) {
        var col: c_int = 0;
        while (col < WINDOW_CONFIG.w) : (col += 1) {
            const color = noise_cache[
                math.index(
                    @intCast(usize, row),
                    @intCast(usize, col),
                    WINDOW_CONFIG.h,
                    WINDOW_CONFIG.w,
                )
            ];

            _ = s.SDL_SetRenderDrawColor(
                renderer,
                color,
                color,
                color,
                255,
            );
            _ = s.SDL_RenderDrawPoint(renderer, col, row);
        }
    }

    if (render_features) renderFeatures(renderer);

    win.should_redraw = false;
}

fn renderFeatures(renderer: *s.SDL_Renderer) void {
    for (worley.features.items) |feature| {
        // _ = s.SDL_RenderDrawPoint(
        // self.ren,
        // @floatToInt(c_int, feature.y),
        // @floatToInt(c_int, feature.x),
        // );
        _ = s.filledCircleRGBA(
            renderer,
            @floatToInt(i16, feature.x),
            @floatToInt(i16, feature.y),
            5,
            255,
            0,
            0,
            255,
        );
    }
}

fn resetCache() void {
    for (noise_cache) |*v| v.* = 0;
    win.should_redraw = true;
}

fn recalculateNoise() void {
    const l = z.log.scoped(.recalculateNoise);
    l.debug("entering recalculateNoise", .{});

    var row: usize = 0;
    while (row < WINDOW_CONFIG.h) : (row += 1) {
        var col: usize = 0;
        while (col < WINDOW_CONFIG.w) : (col += 1) {
            const noise_val = worley.noise(.{
                .x = @intToFloat(f32, col),
                .y = @intToFloat(f32, row),
            }, use_square_distance) catch |e| blk: {
                switch (e) {
                    error.NotEnoughFeatures => {
                        break :blk 0;
                    },

                    else => unreachable,
                }
            };

            const idx = math.index(
                row,
                col,
                WINDOW_CONFIG.h,
                WINDOW_CONFIG.w,
            );
            noise_cache[idx] = @floatToInt(u8, math.remap(
                f32,
                noise_val,
                0,
                max_dist,
                0,
                255,
            ));
        }
    }

    win.should_redraw = true;
}

fn reload() void {
    const l = z.log.scoped(.reload);

    const new_w = Worley.init(alloc, worley.nth_closest);
    worley.deinit();
    worley = new_w;

    l.info("reloading...", .{});

    resetCache();
}
