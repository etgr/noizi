const std = @import("std");

const c = @import("c.zig");
const s = c.sdl;
const l = std.log.scoped(.keyboard);

var state: []const u8 = undefined;

pub fn updateState() void {
    var len: c_int = undefined;
    const new_state = s.SDL_GetKeyboardState(&len);

    state = new_state[0..@intCast(usize, len)];
}

pub fn isKeyDown(comptime key_name: []const u8) bool {
    const kbd = @import("options.zig").options.controls.keyboard;

    const key_code = @field(kbd, key_name);
    const key_scancode = s.SDL_GetScancodeFromKey(key_code);
    const key_idx = @intCast(usize, key_scancode);

    return state[key_idx] != 0;
}
