const z = @import("std");

const Alloc = z.mem.Allocator;
const Rand = z.rand.Random;
const Timer = z.time.Timer;

const sleep = z.time.sleep;

const root = @import("root");

const s = @import("c.zig").sdl;

const keyboard = @import("keyboard.zig");
const mouse = @import("mouse.zig");

pub const Window = struct {
    const Self = @This();

    const TARGET_FPS = 60;
    const TARGET_FRAME_TIME_NS = @truncate(u64, 1_000_000_000 / TARGET_FPS);

    const FONT_FILE = "res/fonts/8bitOperatorPlus/8bitOperatorPlus-Regular.ttf";
    const FONT_SIZE: c_int = 30;

    window: *s.SDL_Window,
    renderer: *s.SDL_Renderer,

    font: *s.TTF_Font,

    running: bool,
    should_redraw: bool,

    pub fn init(
        width: c_int,
        height: c_int,
        title: [*c]const u8,
    ) !Self {
        if (s.SDL_Init(s.SDL_INIT_EVERYTHING) != 0) return error.SdlInit;
        errdefer s.SDL_Quit();

        if (s.TTF_Init() != 0) return error.TtfInit;
        errdefer s.TTF_Quit();

        const win = s.SDL_CreateWindow(
            title,
            s.SDL_WINDOWPOS_CENTERED,
            s.SDL_WINDOWPOS_CENTERED,
            width,
            height,
            s.SDL_WINDOW_SHOWN,
        );
        if (win == null) return error.CreateWindow;
        errdefer s.SDL_DestroyWindow(win);

        const ren = s.SDL_CreateRenderer(win, -1, s.SDL_RENDERER_ACCELERATED);
        if (ren == null) return error.CreateRenderer;
        errdefer s.SDL_DestroyRenderer(ren);

        const fnt = s.TTF_OpenFont(FONT_FILE, FONT_SIZE);
        errdefer s.TTF_CloseFont(fnt);

        return Self{
            .window = win.?,
            .renderer = ren.?,

            .font = fnt.?,

            .running = true,
            .should_redraw = true,
        };
    }

    pub fn deinit(self: Self) void {
        if (comptime rootHasFn("deinit")) root.deinit();

        s.TTF_CloseFont(self.font);
        s.SDL_DestroyRenderer(self.renderer);
        s.SDL_DestroyWindow(self.window);
        s.TTF_Quit();
        s.SDL_Quit();
    }

    pub fn loop(self: *Self) void {
        var timer = Timer.start() catch unreachable;

        while (self.running) {
            timer.reset();

            self.handleEvents();
            self.update(timer.read());
            self.render();

            // After all is done, if there's extra time, sleep until the next
            // frame.
            const ns = timer.lap();
            if (ns < TARGET_FRAME_TIME_NS) sleep(TARGET_FRAME_TIME_NS - ns);

            self.present();
        }
    }

    fn handleEvents(self: *Self) void {
        s.SDL_PumpEvents();

        // == s.SDL_QuitRequested(), but that doesn't work.
        if (s.SDL_PeepEvents(
            null,
            0,
            s.SDL_PEEKEVENT,
            s.SDL_QUIT,
            s.SDL_QUIT,
        ) > 0) {
            self.running = false;
            return;
        }
    }

    fn update(_: Self, elapsed_ns: u64) void {
        keyboard.updateState();
        mouse.updateState();

        if (comptime rootHasFn("update")) root.update(elapsed_ns);
    }

    fn render(self: *Self) void {
        if (!self.should_redraw) return;

        // Set draw color to black and clear the renderer.
        _ = s.SDL_SetRenderDrawColor(self.renderer, 0, 0, 0, 255);
        _ = s.SDL_RenderClear(self.renderer);

        if (comptime rootHasFn("render")) root.render(self.renderer);
    }

    fn present(self: *Self) void {
        s.SDL_RenderPresent(self.renderer);
    }
};

fn rootHasFn(comptime name: []const u8) bool {
    if (@hasDecl(root, name)) {
        if (@typeInfo(@TypeOf(@field(root, name))) != .Fn)
            @compileError("Expected root." ++ name ++ " to be a function");

        return true;
    }

    return false;
}
