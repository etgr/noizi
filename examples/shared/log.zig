const std = @import("std");

pub fn log(
    comptime _: std.log.Level,
    comptime scope: @TypeOf(.EnumLiteral),
    comptime format: []const u8,
    args: anytype,
) void {
    const prefix = "[" ++ @tagName(scope) ++ "] ";

    const printer = std.io.getStdOut().writer();
    nosuspend printer.print(prefix ++ format ++ "\n", args) catch return;
}
