const s = @import("c.zig").sdl;

pub const options = struct {
    const Self = @This();

    controls: struct {
        mouse: struct {
            main: c_int = s.SDL_BUTTON_LEFT,
        } = .{},

        keyboard: struct {
            quit: c_int = s.SDLK_ESCAPE,

            reload: c_int = s.SDLK_r,
            render_features: c_int = s.SDLK_f,
        } = .{},
    } = .{},

    fn init() Self {
        return Self{};
    }
}.init();
