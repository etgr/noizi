const z = @import("std");

// Flatten a 2D index into a linear array.
pub inline fn index(row: usize, col: usize, rows: usize, cols: usize) usize {
    z.debug.assert(row < rows);
    z.debug.assert(col < cols);

    return col + row * cols;
}

test "Index should successfully flatten a 2D array" {
    const MAX_ROWS: usize = 10;
    const MAX_COLS: usize = 15;

    var i: usize = 0;

    var row: usize = 0;
    while (row < MAX_ROWS) : (row += 1) {
        var col: usize = 0;
        while (col < MAX_COLS) : (col += 1) {
            z.debug.assert(index(row, col, MAX_ROWS, MAX_COLS) == i);
            i += 1;
        }
    }
}

// Change the range of a value.
//
// For example, say there's a function that outputs f32s ranging from -1 to 1,
// but it would be more convenient if it went from 0 to 10:
//
//     const new_val = remap(f32, v, -1, 1, 0, 10);
pub fn remap(
    comptime T: type,
    n: T,
    start1: T,
    stop1: T,
    start2: T,
    stop2: T,
) T {
    const max = z.math.max;
    const min = z.math.min;

    const np = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;

    if (np < min(start2, stop2)) {
        return start2;
    } else if (np > max(start2, stop2)) {
        return stop2;
    } else {
        return np;
    }
}

test "Remap should change the range of a value" {
    const expectEqual = z.testing.expectEqual;

    try expectEqual(@as(f32, 0.5), remap(f32, 5, 0, 10, 0, 1));
    try expectEqual(@as(f32, 0.1), remap(f32, 1, 0, 10, 0, 1));
    try expectEqual(@as(f32, 0.7), remap(f32, 7, 0, 10, 0, 1));
    try expectEqual(@as(f32, 1.0), remap(f32, 10, 0, 10, 0, 1));
}
