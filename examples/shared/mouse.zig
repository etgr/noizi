const std = @import("std");

const s = @import("c.zig").sdl;

var state: struct {
    buttons: u32,
    x: c_int,
    y: c_int,
} = undefined;

pub fn updateState() void {
    state.buttons = s.SDL_GetMouseState(&state.x, &state.y);
}

pub fn isButtonDown(comptime button_name: []const u8) bool {
    const mouse_ops = @import("options.zig").options.controls.mouse;

    const sdl_button = @field(mouse_ops, button_name);
    const real_button: u32 = @as(u32, 1) << (sdl_button - 1);

    return state.buttons & real_button > 0;
}

pub fn pos() struct { x: @TypeOf(state.x), y: @TypeOf(state.y) } {
    return .{
        .x = state.x,
        .y = state.y,
    };
}
